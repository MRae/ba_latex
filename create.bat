@echo off

pdflatex raethe11 2>nul
bibtex raethe11 2>nul
pdflatex raethe11 2>nul
makeindex -s raethe11.ist -t raethe11.alg -o raethe11.acr raethe11.acn
pdflatex raethe11 2>nul
pdflatex raethe11 2>nul
makeindex -s raethe11.ist -t raethe11.glg -o raethe11.gls raethe11.glo
makeindex -s raethe11.ist -t raethe11.alg -o raethe11.acr raethe11.acn
pdflatex raethe11 2>nul
pdflatex raethe11 2>nul
pdflatex raethe11 2>nul

REM PDF mit 128-Bit verschlüsseln und nur drucken erlauben. Für Online-Publikation.

E:

cd E:\Eigene Dateien\My Dropbox\Bachelorarbeit\BachelorThesis

pdftk raethe11.pdf output raethe11_1.pdf owner_pw Grünwal$er60! allow printing