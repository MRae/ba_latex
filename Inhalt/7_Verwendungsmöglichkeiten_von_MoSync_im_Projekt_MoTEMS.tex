\chapter{Verwendungsmöglichkeiten von MoSync im Projekt MoTEMS}
\label{chap:Verwendungsmöglichkeiten von MoSync im Projekt MoTEMS}
Ziel dieses Kapitels ist es, die im vorangegangen \fref{chap:Anwendung der Kommunikationsschnittstellen} verwendeten plattformübergreifenden Schnittstellen in Java zur Verfügung zu stellen und somit eine Integration in die Entwicklungsumgebung von \gls{MoTEMS} zu realisieren. Anschließend sollen weitere potenzielle Einsatzmöglichkeiten von MoSync im Projekt \gls{MoTEMS} betrachtet werden.


\section{Integration der Schnittstellen}
\label{sec:Integration der Schnittstellen}
Im Folgendem soll nach Lösungsmöglichkeiten gesucht werden, die in MoSync verfügbaren plattformunabhängigen Kommunikationsschnittstellen in die Entwicklungsumgebung von \gls{MoTEMS} zu integrieren. Somit soll beispielsweise eine plattformübergreifende und standardisierte Bluetooth-Kommunikation zwischen einer Teststation und verschiedenen mobilen Endgeräten gewährleistet werden.

\subsection{Java Native Interface}
\label{sec:Java Native Interface}
Mit dem \gls{JNI} bietet Java eine standardisierte Anwendungsschnittstelle zum Aufruf plattformspezifischer beziehungsweise nativer Bibliotheken und Anwendungen aus der Programmiersprache Java heraus. So ermöglicht es \gls{JNI} beispielsweise, dass ein Java-Programm Funktionen einer Windows-\gls{DLL}, die in C{}\verb!++! programmiert ist, aufruft. Sowohl die \gls{JVM} als auch die native Anwendung befinden sich hierbei im gleichen Adressraum. Ein Vorteil dieser Tatsache ist, dass keine aufwendige \gls{IPC} benötigt wird, um die Funktionalität der jeweils anderen Seite nutzen zu können. \cite[S.\,32]{ofterdinger05}

Der beschriebene Ansatz ist allerdings nicht auf MoSync anwendbar. Schaut man sich die Schritte an, um nativen Code direkt aus Java aufrufen zu können, wird dies ersichtlich:
\begin{enumerate}
\item Schreiben der Java-Klasse
\item Kompilieren der Java-Klasse
\item Generieren einer C Header-Datei aus der Java-Klasse
\item Implementierung der nativen Funktion in C{}\verb!++!
\item Kompilieren und Linken der nativen Quellen zu einer \gls{DLL}
\end{enumerate}

Spätestens im letzten Schritt wird MoSync den Anforderungen nicht gerecht. \glspl{DLL} können Programmcode in Form von Maschinencode und sonstige Ressourcen enthalten. Würde man nun versuchen mit dem MoSync Compiler eine \gls{DLL} zu erzeugen, so würde in diese jedoch keine Maschinenbefehle, sondern nur \gls{MoSyncIL} Anweisungen enthalten (siehe \fref{sec:Erstellungsprozess}). Keine \gls{Plattform} wäre in der Lage diese Bibliothek zu interpretieren. Eventuell würde dieser Ansatz funktionieren, wenn zusätzlich das Pipe-Tool und eine entsprechende MoSync \gls{Laufzeitumgebung} in die \gls{DLL} integriert werden würden. Jeder Aufruf einer Funktion innerhalb der \gls{DLL} müsste dann wiederum zuerst an das Pipe-Tool und dann an die \gls{Laufzeitumgebung} übergeben werden, welche dann in der Lage wäre den vom Pipe-Tool erzeugten Bytecode in Maschinencode zu übersetzen und auszuführen.

Eine Realisierung dieser Vorgehensweise scheint wenn überhaupt nur mit unverhältnismäßigem Aufwand möglich.

\subsection{Integration einer JAR-Datei}
\label{sec:Integration einer JAR-Datei}
Eine JAR-Datei oder ein Java Archiv ist eine ZIP-Datei, in der alle benötigten Klassen, Referenzen, Bilder und Ressourcen einer Java-Klasse oder eines Java-Programms enthalten sind \cite{oracle11}. Diese lassen sich für eine Wiederverwendbarkeit in verschiedene Java-Entwicklungsumgebungen integrieren. Somit ist es möglich, deren verfügbare Klassen zu instanzieren und auf deren Methoden zuzugreifen.

MoSync ist in der Lage diese JAR-Dateien für verschiedene Endgeräte mit Java ME zu erstellen. Allerdings sind auch diese für eine Einbindung in eine Java-Entwicklungsumgebung nutzlos. Beschreiben zwar einige Teile der MoSync Dokumentation eine Kompilierung des C{}\verb!++! Quellcodes direkt in Java Bytecode, so ist diese Variante aktuell nicht implementiert. Die eigentliche Anwendung liegt auch hier als MoSync Bytecode vor. Alle Dateien, die neben diesem Bytecode im Archiv vorliegen, sind Java Bytecode und repräsentieren den für Java ME benötigten VM Core (siehe \fref{sec:Core}).

Somit kann auch mit diesem Einsatz die gewünschte Nutzung der Schnittstellen aus MoSync nicht realisiert werden.

\subsection{Interprozesskommunikation}
\label{sec:Interprozesskommunikation}
Unter \gls{IPC} versteht man Methoden zum Informationsaustausch von nebenläufigen Prozessen oder Threads. Im engeren Sinne realisiert sie die Kommunikation zwischen Prozessen auf demselben Computer, deren Speicherbereiche aber strikt voneinander getrennt sind. Im weiteren Sinne bezeichnet \gls{IPC} aber jeden Datenaustausch in verteilten Systemen, angefangen bei Threads, die sich ein Laufzeitsystem teilen, bis hin zu Programmen, die auf unterschiedlichen Rechnern laufen und über ein Netzwerk kommunizieren (\gls{Socket}).

Der Ansatz der \gls{IPC} erfordert eine Nebenläufigkeit von Prozessen beziehungsweise Threads, was von vielen mobilen \glspl{Plattform} auf Betriebssystemebene nur begrenzt beziehungsweise nicht programmübergreifend unterstützt wird.

\section{Allgemeine Einsatzmöglichkeiten}
\label{sec:Allgemeine Einsatzmöglichkeiten}
Nachdem in \fref{sec:Integration der Schnittstellen} keine Lösung für eine Integration der Kommunikationsschnittstellen in die \gls{MoTEMS}-Entwicklungsumgebung beziehungsweise Java gefunden werden konnte, sollen nach weiteren Einsatzmöglichkeiten von MoSync im Projekt \gls{MoTEMS} gesucht werden.

\subsection{Prototyping}
\label{sec:Prototyping}
Eine weitere Anwendungsmöglichkeit von MoSync und anderen plattformübergreifenden Entwicklungskits stellt das experimentelle Prototyping dar. Bei diesem Vorgehen wird zu Forschungszwecken oder zur Suche nach Realisierungsmöglichkeiten ein experimenteller Prototyp entwickelt \cite[S.\,536]{claus06}. Durch die Verwendung der zur Verfügung stehenden High-Level-\glspl{API} und dem daraus reduzierten Aufwand können Anwendungen zeitnah auf mehreren \glspl{Plattform} verfügbar gemacht und somit einer Problemanalyse unterzogen werden. Die gewonnenen Erkenntnisse können anschließend für eine Weiterentwicklung verwendet werden.

\subsection{Steuerung von Endgeräten}
\label{sec:Steuerung von Endgeräten}
Eine weitere potenzielle Einsatzmöglichkeit von Cross-Platform-Frameworks könnte die Steuerung verschiedener Endgeräte darstellen. Derzeit werden solche \glspl{API} von MoSync und auch von anderen in \fref{sec:Typisierung mobiler Applikationen} aufgeführten \glspl{SDK} nicht angeboten.